## Registre Docker

Ce dossier contient les ressources nécessaires pour lancer un registre Docker privé.
Ce registre sert à stocker l'ensemble des images Docker construites par Picasoft.

Il est accessible via HTTPS, donc par Traefik, et ne nécessite donc pas la création manuelle de certificats.

La connexion se fait via les comptes LDAP.

### Configuration

Aucune configuration n'est à faire pour le registre. La configuration pour vérifier les comptes LDAP est à retrouver sur la [page dédiée](../pica-nginx-ldap).

### Mise à jour

Il suffit de changer le tag dans [Compose](./docker-compose.yml). En cas de mise à jour majeure (2 -> 3), regarder les instructions correspondantes sur le [Docker Hub](https://hub.docker.com/_/registry/).
