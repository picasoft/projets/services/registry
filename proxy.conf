# disable any limits to avoid HTTP 413 for large image uploads
client_max_body_size 0;

# required to avoid HTTP 411: see Issue #1486 (https://github.com/moby/moby/issues/1486)
chunked_transfer_encoding on;

location /v2 {
    add_header 'Docker-Distribution-Api-Version' 'registry/2.0';

    proxy_pass                          http://registry:5000;
    proxy_set_header  Host              $http_host;   # required for docker client's sake
    proxy_set_header  X-Real-IP         $remote_addr; # pass on real client's IP
    proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
    # Forwarding from Traefik, always https
    proxy_set_header  X-Forwarded-Proto https;
    proxy_read_timeout                  900;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection keep-alive;
    proxy_request_buffering off;
    proxy_cache off;
    proxy_buffering off;
}
